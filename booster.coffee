(((func) ->
  script = document.createElement('script')
  script.setAttribute('type', 'text/javascript')
  script.textContent = "(#{func.toString()})();"
  document.body.appendChild(script)
  document.body.removeChild(script)
  return
)(->
  class Booster
    @ID: 'tw_booster'
    @NAME: 'Booster'
    @AUTHOR: 'neversleep1911'
    @WEB_SITE: 'https://greasyfork.org/scripts/9007'
    @MIN_GAME_VERSION: '2.21'
    @MAX_GAME_VERSION: Game.version.toString()

    INTERVAL: 20000

    constructor: ->
      @timer = setInterval(Chat.Request.Nop, @INTERVAL)

  $(document).ready(->
    api = TheWestApi.register(
      Booster.ID,
      Booster.NAME,
      Booster.MIN_GAME_VERSION,
      Booster.MAX_GAME_VERSION,
      Booster.AUTHOR,
      Booster.WEB_SITE
    )
    api.setGui("Copyrights, changelog and other details see <a href='#{Booster.WEB_SITE}' target='_blank'>here</a>.")

    new Booster()
    true
  )
  return
))
