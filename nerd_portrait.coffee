(((func) ->
  script = document.createElement('script')
  script.setAttribute('type', 'text/javascript')
  script.textContent = "(#{func.toString()})();"
  document.body.appendChild(script)
  document.body.removeChild(script)
  return
)(->
  class I18n
    DEFAULT_LANGUAGE = 'en_US'
    STRINGS =
      en_US:
        buttons:
          save: 'Save'
        messages:
          text_saved: 'Text saved'
          enter_text: 'Enter text!'

      de_DE:
        buttons:
            save: "Speichern"
        messages:
            text_saved: "Text gespeichert"
            enter_text: "Text eingeben!"

      ru_RU:
        buttons:
          save: 'Сохранить'
        messages:
          text_saved: 'Текст сохранён'
          enter_text: 'Введите текст!'

    language = DEFAULT_LANGUAGE

    @language: ->
      language

    @setLanguage: (lang) ->
      language = if STRINGS[lang] then lang else DEFAULT_LANGUAGE

    @tr: (id) ->
      string = STRINGS[language]
      properties = id.split('.')
      for prop in properties
        break if (string = string[prop]) == undefined
      string || id

  class Gui
    @createButton: (options = {}) ->
      if options.icon
        button = new west.gui.Iconbutton(options.icon, options.onclick)
      else
        button = new west.gui.Button(options.text, options.onclick)

      $(button.getMainDiv()).css(options.css) if options.css
      button

    @createTextarea: (options = {}) ->
      textarea = new west.gui.Textarea(options.content, null)
      textarea.setWidth(options.width) if options.width
      textarea.setHeight(options.height) if options.height
      $(textarea.getMainDiv()).css(options.css) if options.css
      textarea

    @createGroup: (options = {}) ->
      group = new west.gui.Groupframe('', '<div />')
      $(group.getMainDiv()).css(options.css) if options.css

      content = $('.tw2gui_groupframe_content_pane div', group.getMainDiv()).first()
      content.append($("<h2>#{options.title}</h2><hr style='margin-top: 2px; margin-bottom: 4px;' />")) if options.title

      if options.scrollPane
        scrollPane = new west.gui.Scrollpane()
        $(scrollPane.getMainDiv()).css(options.scrollPane.css) if options.scrollPane.css

        content.append(scrollPane.getMainDiv())

        group.scrollPane = scrollPane
        group.appendToScrollContentPane = (content) ->
          scrollPane.appendContent(content)
          group

      group

  class NerdFormatter
    @keys: [
      # Health regeneration
      {
        pattern: /%ch/
        value: -> Character.healthRegen
      },

      # Energy regeneration
      {
        pattern: /%ce/
        value: -> Character.energyRegen
      },

      # %hp - Health percent
      {
        pattern: /%hp/
        value: -> Math.round(Character.health * 100 / Character.maxHealth)
      },

      # %hr - 1HP regeneration time
      {
        pattern: /%hr/
        value: -> Math.round(1 / (Character.healthRegen * Character.maxHealth) * 3600).formatDuration()
      },

      # %ht - Time to full health regeneration
      {
        pattern: /%ht/
        value: ->
          ((Character.maxHealth - Character.health) / (Character.healthRegen * Character.maxHealth) * 3600).formatDuration()
      },

      # %ep - Energy percent
      {
        pattern: /%ep/
        value: -> Math.round(Character.energy * 100 / Character.maxEnergy)
      },

      # %er - 1EP regeneration time
      {
        pattern: /%er/
        value: -> Math.round(1 / (Character.healthRegen * Character.maxEnergy) * 3600).formatDuration()
      },

      # %et - Time to full energy regeneration
      {
        pattern: /%et/
        value: ->
          ((Character.maxEnergy - Character.energy) / (Character.energyRegen * Character.maxEnergy) * 3600).formatDuration()
      },

      # %dl - Duel level
      {
        pattern: /%dl/
        value: -> Character.duelLevel
      },

      # %dm - Duel level min
      {
       pattern: /%dm/
       value: -> Math.ceil(5 * Character.duelLevel / 7)
      },

      # %dx - Duel level max
      {
        pattern: /%dx/
        value: -> Math.ceil(7 * Character.duelLevel / 5)
      }
    ]

    @format: (s) ->
      for k in @keys
        s = s.replace(k.pattern, k.value())
      s

  class NerdPortrait
    @ID: 'tw_nerd_portrait'
    @NAME: 'NERD Portrait'
    @AUTHOR: 'neversleep1911'
    @WEB_SITE: 'https://greasyfork.org/scripts/9505'
    @MIN_GAME_VERSION: '2.22'
    @MAX_GAME_VERSION: Game.version.toString()

    @LSK_TEXT: '3577cb3d-1fbf-4c37-9b6a-3854a6c4c34d'

    @DEFAULT_TEXT: '<div style="color: whitesmoke;
                                position: relative;
                                left: 10px;
                                top: 5px;
                                font-size: 75%;
                                font-weight: bold;
                                line-height: 110%;">\n' +
                  'Health: %hp%<br />\n' +
                  'Regen: %ch (%ht)<br />\n' +
                  '1HP / %hr<br /><br />\n\n' +
                  'Energy: %ep%<br />\n' +
                  'Regen: %ce (%et)<br />\n' +
                  '1EP / %er<br /><br />\n\n' +
                  'Duel Level: %dl (%dm-%dx)' +
                  '</div>'

    instance = null

    constructor: ->
      return instance if instance

      instance = @

      EventHandler.listen([
        'health',
        'energy',
        'wear_changed',
        'taskqueue-updated',
        'position_change',
        'character_level_up'
      ], ->
        NerdPortrait.instance().update()
        return
      )
      @update()
      return

    @instance: ->
      instance

    update: ->
      $('div.character_link').append(@content = $("<div />")) unless @content
      @content.html(NerdFormatter.format(@text()))
      return

    text: ->
      @_text || (@_text = localStorage.getItem(NerdPortrait.LSK_TEXT)) || NerdPortrait.DEFAULT_TEXT

    setText: (@_text) ->
      localStorage.setItem(NerdPortrait.LSK_TEXT, @_text)
      @update()
      return

  class Tab
    instance = null

    constructor: ->
      return instance if instance

      instance = @

      @maindiv = $('<div />')

      @maindiv.append((@textGroup = Gui.createGroup()).getMainDiv())
      @maindiv.append((@actionGroup = Gui.createGroup()).getMainDiv())

      @textGroup.appendToContentPane((@text = Gui.createTextarea(
        width: 598
        height: 205
      )).getMainDiv())

      @actionGroup.appendToContentPane((@buttonSave = Gui.createButton(
        text: I18n.tr('buttons.save')
      )).getMainDiv())

      EventHandler.listen('WINDOW_OPENED', (uid) ->
        return if uid != 'scripts'

        Tab.instance().text.setContent(NerdPortrait.instance().text())

        $(Tab.instance().buttonSave.getMainDiv()).off('click').on('click', ->
          text = $.trim(Tab.instance().text.getContent())
          if text != ''
            try
              NerdPortrait.instance().setText(text)
              new UserMessage(I18n.tr('messages.text_saved'), UserMessage.TYPE_SUCCESS).show()
            catch e
              new UserMessage(e, UserMessage.TYPE_ERROR).show()
          else
            new UserMessage(I18n.tr('messages.enter_text'), UserMessage.TYPE_HINT).show()

          false
        )
        return
      )

    @instance: ->
      instance

  $(document).ready(->
    I18n.setLanguage(Game.locale)

    api = TheWestApi.register(
      NerdPortrait.ID,
      NerdPortrait.NAME,
      NerdPortrait.MIN_GAME_VERSION,
      NerdPortrait.MAX_GAME_VERSION,
      NerdPortrait.AUTHOR,
      NerdPortrait.WEB_SITE
    )
    api.setGui((new Tab).maindiv)

    new NerdPortrait()
    true
  )
  return
))
