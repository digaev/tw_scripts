(((func) ->
  script = document.createElement('script')
  script.setAttribute('type', 'text/javascript')
  script.textContent = "(#{func.toString()})();"
  document.body.appendChild(script)
  document.body.removeChild(script)
  return
)(->
  class I18n
    DEFAULT_LANGUAGE = 'en_US'
    STRINGS =
      de_DE:
        dialogs:
            add_item: "Gegenstand hinzufügen",
            add_character_items: "Items eines Spielers"
        groups:
            skills: "Fertigkeiten",
            items: "Ausrüstung"
        buttons:
            character: "Spieler",
            add: "Hinzufügen",
            reset: "Zurücksetzen"
        checkboxes:
            show_bonus: "Bonus anzeigen",
            show_skills: "Meinen Skill anzeigen"
        labels:
            level: "Stufe",
            item_id: "Item ID",
            character_name: "Name des Spielers",
            health: "LPs"
        tooltip:
            health: "Normal / Soldat / Soldat mit Bonus"
        errors:
            player_not_found: "Spieler nicht gefunden!"

      en_US:
        dialogs:
          add_item: 'Add item'
          add_character_items: "Character's items"
        groups:
          skills: 'Skills'
          items: 'Items'
        buttons:
          character: 'Character'
          add: 'Add'
          reset: 'Reset'
        checkboxes:
          show_bonus: 'Show bonus'
          show_skills: 'Show my skills'
        labels:
          level: 'Level'
          item_id: 'Item ID'
          character_name: "Character's name"
          health: 'Health'
        tooltip:
          health: 'Normal / Solder / Solder with bonus'
        errors:
          player_not_found: 'Player not found!'

      pl_PL:
        dialogs:
            add_item: "Dodaj przedmiot",
            add_character_items: "Przedmioty gracza"
        groups:
            skills: "Umiejętności",
            items: "Przedmioty"
        buttons:
            character: "Gracz",
            add: "Dodaj",
            reset: "Reset"
        checkboxes:
            show_bonus: "Pokaż bonus",
            show_skills: "Pokaż moje umiejętności"
        labels:
            level: "Level",
            item_id: "ID przedmiotu",
            character_name: "Nazwa gracza",
            health: "Życie"
        tooltip:
            health: "Normalny / Żołnierz / Żołnierz z bonusem"
        errors:
            player_not_found: "Gracz nie znaleziony!"

      ru_RU:
        dialogs:
          add_item: 'Добавить предмет'
          add_character_items: 'Предметы персонажа'
        groups:
          skills: 'Навыки'
          items: 'Предметы'
        buttons:
          character: 'Персонаж'
          add: 'Добавить'
          reset: 'Сбросить'
        checkboxes:
          show_bonus: 'Показывать бонусы'
          show_skills: 'Показывать мои навыки'
        labels:
          level: 'Уровень'
          item_id: 'ID предмета'
          character_name: 'Имя персонажа'
          health: 'Здоровье'
        tooltip:
          health: 'Минимум / Солдат / Солдат с бонусом'
        errors:
          player_not_found: 'Игрок не найден!'

    language = DEFAULT_LANGUAGE

    @language: ->
      language

    @setLanguage: (lang) ->
      language = if STRINGS[lang] then lang else DEFAULT_LANGUAGE

    @tr: (id) ->
      string = STRINGS[language]
      $(id.split('.')).each (k, v) ->
        (string = string[v]) != undefined
      string || id

  class Gui
    @createMenuButton: (options = {}) ->
      button = $("<div class='menulink' title='#{options.title}' />")
      button.css('background-image', "url(#{options.image})") if options.image
      button.hover(
        (-> $(@).css('background-position', '-25px 0px'); true),
        (-> $(@).css('background-position', '0px 0px'); true)
      ).on('click', options.onclick)

      $('div#ui_menubar').append($('<div class="ui_menucontainer" />').append(button).append('<div class="menucontainer_bottom" />'))
      button

    @createGroup: (options = {}) ->
      group = new west.gui.Groupframe('', '<div />')
      $(group.getMainDiv()).css(options.css) if options.css

      content = $('.tw2gui_groupframe_content_pane div', group.getMainDiv()).first()
      content.append($("<h2>#{options.title}</h2><hr style='margin-top: 2px; margin-bottom: 4px;' />")) if options.title

      if options.scrollPane
        scrollPane = new west.gui.Scrollpane()
        $(scrollPane.getMainDiv()).css(options.scrollPane.css) if options.scrollPane.css

        content.append(scrollPane.getMainDiv())

        group.scrollPane = scrollPane
        group.appendToScrollContentPane = (content) ->
          scrollPane.appendContent(content)
          group

      group

    @createButton: (options = {}) ->
      if options.icon
        button = new west.gui.Iconbutton(options.icon, options.onclick)
      else
        button = new west.gui.Button(options.text, options.onclick)

      $(button.getMainDiv()).css(options.css) if options.css
      button

    @createCheckbox: (options = {}) ->
      checkbox = new west.gui.Checkbox(options.text, '', options.onclick)
      $(checkbox.getMainDiv()).css(options.css)
      checkbox

    @createTextfield: (options = {}) ->
      field = new west.gui.Textfield()
      field.setSize(options.size)
      field.setLabel($("<span>#{options.label}</span>"))
      field.setValue(options.value) if options.value
      $(field.getMainDiv()).css(options.css) if options.css
      field

  class SkillsCalc
    @ID: 'tw_skills_calc'
    @NAME: 'Skills Calc'
    @AUTHOR: 'neversleep1911'
    @WEB_SITE: 'https://greasyfork.org/scripts/7829'
    @MIN_GAME_VERSION: '2.21'
    @MAX_GAME_VERSION: Game.version.toString()
    MENU_BUTTON_IMAGE: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAZCAIAAAD8NuoTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QQICBgbVC0rcwAACI5JREFUSMel10tvXFcBAODzuq+5987MHY/nYTvx24lj13k0KQ5NmjR90ELVVoIKSitARWxAFSyqskKskLphwQKJFUKilUCIBVIXFVSN0tKmTVPbbdwkjhOPXzPjGd953vc9557Dgp8wf+Dbf/A3PzpZr9m5wum7ew+E45maEau+nphaOgwYzBqWE0WdXldLWF/iFiCAiYyebXa9nUbbjkNAcN5IJ0FUzmpL87l6LcyXypW9Wux76WzOdblhcaIBHqpZM+X73O32uBwyCrmaGJTIKb1l9xpdp+VTpCADK0Qkw2kF/vZ7p48tnH7zrb/UEw4ZAAQxLrCQhJA4DIlIEoAEEjLHMUSKSMW4iwAWPOECaUSlLBhJg1+/8dPmyodYTY3PnF+7vd6MXAFBwmIFmgD0w5BIipaoLosQEZCRSIksriYgcaFAnKMEJJIkYRhnFfPM4kJl5WP42hNz9zYas6cnVQP2Omx/rxNxIKeU9HCxnNYuP/aYnskW86n1O41/vvv+5pc3MUYSQQFLKBcYAiBAkoiTJyZ//tz8v95fWbtvLywNRH3n/MnnL5Tga08c+2Tl3ty5b2ZzOe53GaVKSpcVOUlY1G712n2Js8jxy4Y6kTc5Rm7IXInU7P6n24c84UVDVQjad9pvvv7K/Tv3rn2xNzM7PQiVyZvPP/8kkUvTG9373s1VAQAVYFgmZ4sZgrGlEBUhT0W6ZoAhsxfGn9e6LQBOjFrLC+Ndjj7Z+k/Li2nCRywd+0q9Xp2YmLh2s7I6GDWby+xubZKhMVUiXJZJTLklkyPZ1Mf1bsDFuKnO5E0VIwWAbbt/y3ZeeejI0089nD8+4dzZys1N5IvDb/zhXRH2I5oUCnm1VesEkcphNBiFk82COY1PWWhnrxsJwBKOIPRposskRTCHsOGEaVXe9ughgJamVDm+H2p2vTe9MJudndM1hPPj318amjalzWb3Zy8+UtltVJq27bBBqJbtXVy+RFKCmhjbjKsSkgkCECQQRkKUDO2g4+72/FcvLz77wqU9FxqHNWXIGiqXuQgwdVzXG7WkV0994+1/fKCpbjOqZS3dDTFj8SCU3bMODxrILBYpogIILgDnIi0RGYCcIilAICBOF8zlh+cbd++OTU/v3a04bTvkMVK0pNu0NFL2Gq7rFpYWi2a/fTDVFRbkwYAUkAQFHCWpnJAkhJCAIKAsSrhB8PJM8cx0Ka3IkqJyDgFDW7c2xs4vsjC8/+H1+tpa6ERhgvpBIFsFynDkRx3vy1wBSHoyIMUjCuKYJDw0JMwEdAKaIigRoh1RgYAXxAQBM59e94nkRp59a8wkaQIalWbQdTVVjhHmCeJqdqv2VbvbPzrxzL3aVuxDTSaDUHajIS/M4EePZ1fu2C9+a9LUFQkRxjiGoG67mHOaiI1mN2o1G4H47oXjnUYrlZJHZkeMrIIQAFFYKpaUI+Mra6th3z81u9RsB/+9ufXSczODUFZKOX5illi62fejZ58c9rz87Vvue1f3XT+EBAYxQwi8/Pip5TOzSemEgf15UxxWbGt6xKs3+o2+H0b7lH/W+KjV2w0pkFHnWFbmCA9Iuf2oHVDitx2aIEVKZAuMj8u6jps9birQS7ipkHqze39zO+yi21iZSaOR6el+o51EIAxZ4NAudK7euDE7Jd3cqFzZmavWtoRIBqSuXt8cKg+jSvUwEAwJGEXCyKKHlsyUQSIuFIJdxu9sVGOXikZ1OKtHSp4K0d1t0Jh2O06CycLinKKQfE5hHK6u73tB6MTJgFRIycGhg8YfOsoEhxgyAASC5XJWVaWIJgaGiItDmgQdp8zCocOq7reqn3+FsMwoS/wYCUF933X9fj9OIJg7l5u7cjYBYkAqk1GPlCdJSs/mlfTeNq1UQ8fhgR1Rj8aM9WIGILQgmBofGr/8dO2rz6Dt8oiFfV+TQMaUQ8EnJos//uFLURSUU7tHi8refjCclQekDPy3ctEiO1/vTI2of/rrfT9OZIwJQhBCDmEnYkCI+bwu0kYQBIVT56K9u2EYeEF89d5BXsLHJ4f9+s78VAnynNvvr20dOrVg6sjwgFRMnPWtVXzpzNint/Y9j3djGsXMiRngnCci4gJBEDHRbnTt2/cmDWEa5IvP773z2YPF+bHzy8dYEAFFpyniNR9cu75tDRsToxPvvLcOmRiEWllrEijIdCkrUzF6NI3iOJ3WCAAEw2xakmJhO7TS867v2Tdo42qlHiUCSuhXP7gwM1nAkqrlM7erdGNdefyIsji/lCe9vCkmdVwqDUQtnzhmGAhVIzWVMYZKWgSgwxIfQpdyR6COqo4eyZ4rWzMZ/eyIdeiF375y8ve/eHKIRf1aP05gulwMws6DrV25sLR3sK9ocqUjkRQekNrYvtXo7JIPPt7YbLYvXixtbUMci5iyWEDgs0yK1+sxZ0JDKEDwlxePTYxl9lf3s6M5WVc5hI1PbxwvFeauvPLFtfe3q3sCNLGob9r+5YI8CHXzrt30BXnh4kSvc6iloCYRjzFM0KyhDClS7NE2FRAIxrkI6epWK+qEw4WMbhGc0/lBzTzxjDR9KuSqXhrLl5TJscKl+VOxWB2QmpuckVUB//76+UOReevtf9u+NDWUcnicVlXap1wDQQB1whAGcSQ4QBzLUhxxCLR0yqUBTyK7z9OUT0zpjz991v16ZfmRea4u/fHP7+xGIiOritZXuc7jSFItGrbqkRg1VcejwsgYMQNy3Le5NGRmM1Jlx85IYqMVLS2P/mSpWK3uwN+9/EgaoY1Go+6y0VKx7UT1jud6Hog9azivQZ4GwJCArOB0SklZWJWBFwotV/Tttt13uGg9dWFaTrpr2xZ1SYaA7S7dPTwYGRn1YlFttnuURf12TtUky8i5sJghHg7KukZ0nM7+/zsAkbGuvSM1e5OPphNjaGd3/38JylRK90rXAwAAAABJRU5ErkJggg=='

    windows: {}

    constructor: ->
      self = @
      I18n.setLanguage(Game.locale)

      Gui.createMenuButton(
        title: SkillsCalc.NAME
        image: @MENU_BUTTON_IMAGE
        onclick: (e) ->
          self.createWindow()
          e.preventDefault()
          false
      )

    createWindow: ->
      self = @

      loop
        wndId = Math.ceil(Math.random() * 1024)
        break unless @windows[wndId]

      @windows[wndId] = new Window(id: wndId, title: SkillsCalc.NAME, miniTitle: SkillsCalc.NAME)
      @windows[wndId].onDestroy = (wnd) ->
        delete self.windows[wnd.id]
        true
      true

    class Window
      constructor: (options) ->
        self = @

        @images = {}
        @items = {}
        @calculator = new ItemCalculator
        @calculator.level = Character.level

        @id = options.id
        @wnd = wman.open("skills-calc-window-#{options.id}", null, 'noreload')
        @wnd.setTitle(options.title)
        @wnd.setMiniTitle(options.miniTitle)
        @wnd.addEventListener('WINDOW_DESTROY', ->
          if self.onDestroy then self.onDestroy(self) else true
        )

        @wnd.appendToContentPane((@groupSkills = Gui.createGroup(
          title: I18n.tr 'groups.skills'
          css:
            width: 402
            position: 'absolute'
          scrollPane:
            css:
              height: 264
        )).getMainDiv())

        @wnd.appendToContentPane((@groupItems = Gui.createGroup(
          title: I18n.tr 'groups.items'
          css:
            left: 400
            width: 294
          scrollPane:
            css:
              height: 264
        )).getMainDiv())

        @wnd.appendToContentPane(Gui.createButton(
          text: I18n.tr 'buttons.reset'
          css:
            left: 592
            top: 342
            position: 'absolute'
          onclick: (button, data) -> self.onButtonResetClick(button, data)
        ).getMainDiv())

        @wnd.appendToContentPane(Gui.createButton(
          text: I18n.tr 'buttons.add'
          css:
            left: 490
            top: 342
            position: 'absolute'
          onclick: (button, data) -> self.onButtonAddClick(button, data)
        ).getMainDiv())

        @wnd.appendToContentPane(Gui.createButton(
          text: I18n.tr 'buttons.character'
          css:
            left: 388
            top: 342
            position: 'absolute'
          onclick: (button, data) -> self.onButtonCharacterClick(button, data)
        ).getMainDiv())

        @wnd.appendToContentPane((@checkboxBonus = Gui.createCheckbox(
          text: I18n.tr 'checkboxes.show_bonus'
          css:
            left: 2
            top: 324
            position: 'absolute'
          onclick: (state) -> self.onCheckboxBonusClick(state)
        )).getMainDiv())

        @wnd.appendToContentPane((@checkboxSkills = Gui.createCheckbox(
          text: I18n.tr 'checkboxes.show_skills'
          css:
            left: 2
            top: 350
            position: 'absolute'
          onclick: (state) -> self.onCheckboxSkillsClick(state)
        )).getMainDiv())

        @wnd.appendToContentPane((@textfieldLevel = Gui.createTextfield(
          size: 6
          label: I18n.tr 'labels.level'
          value: @calculator.level
          css:
            left: 180
            top: 320
            position: 'absolute'
        )).getMainDiv())

        @wnd.appendToContentPane(@labelHealth =
          $('<span />').css(
            position: 'absolute'
            left: 180
            top: 352
          ).attr('title', I18n.tr('tooltip.health'))
        )

        @textfieldLevel.onlyNumeric().getField().keyup((e)->
          level = parseInt(self.textfieldLevel.getValue())
          if !isNaN(level) && level > 0
            self.calculator.level = level
            self.recalc()
            self.repaint()
          true
        )

        @initGroupSkills()
        @initGroupItems()

      initGroupSkills: ->
        attrs = CharacterSkills.allAttrKeys
        for attr in attrs
          div = $('<div style="height: 41px;" />')
          skills = CharacterSkills.skillKeys4Attr[attr]
          for skillType in skills
            skill = new Skill(skillType)
            img = skill.getSkillImage()

            img.removeAttr('class').css(
              width: 72,
              display: 'inline-block'
              'text-align': 'center'
              'font-weight': 'bold',
              'margin-left': 2
            )
            $('img.skillicon', img).removeAttr('class').css(width: '100%')
            $('span.skillpoints_label', img).attr('class', 'skills-calc-skillpoints_label').css(
              display: 'inline-block'
              position: 'relative'
              top: -16
              width: '100%'
              height: 12
              color: '#ffffff'
              'text-align': 'center'
              'font-size': '9pt'
              'text-shadow': '1px 1px 1px rgb(0, 0, 0)'
              'background-image': "url('/images/tw2gui/plusminus/plusminus_display_bg2.png')"
            )

            div.append(img)
            @images[skillType] = img.get(0)

          @groupSkills.appendToScrollContentPane(div)
          @groupSkills.appendToScrollContentPane($('<hr style="margin: 12px 0;" />'))

        true

      initGroupItems: ->
        items = []
        for slot in Wear.slots
          items.push(Wear.wear[slot].obj) if Wear.wear[slot]

        @addItems(items)
        @repaint()

      onButtonCharacterClick: ->
        self = @
        dlg = new west.gui.Dialog(I18n.tr('dialogs.add_character_items'))
        content = $("<div style='margin-top: 10px; text-align: center;'></div>")
        content.append((textfieldName = Gui.createTextfield(size: 25, label: I18n.tr('labels.character_name'))).getMainDiv())

        ok = ->
          return false unless name = $.trim(textfieldName.getValue())

          self.wnd.showLoader()

          Ajax.remoteCallMode('ranking', 'get_data', {
            rank: NaN,
            search: name = name.toLowerCase(),
            tab: 'duels'
          }, (json) -> (
            if json.error
              self.wnd.hideLoader()
              return new UserMessage(json.msg, UserMessage.TYPE_ERROR).show()

            found = false

            for player in json.ranking
              found = player.name.toLowerCase() == name
              if found
                Ajax.remoteCallMode('profile', 'init', {
                  name: player.name,
                  playerId: player.id
                }, (resp) ->
                  if resp.error
                    self.wnd.hideLoader()
                    return new UserMessage(resp.message, UserMessage.TYPE_ERROR).show()
                  else
                    items = []
                    for slot in Wear.slots
                      items.push(ItemManager.get(resp.wear[slot])) if resp.wear[slot]

                    self.reset()
                    self.calculator.level = resp.level
                    self.textfieldLevel.setValue(resp.level)
                    self.addItems(items)
                    self.repaint()
                    self.wnd.hideLoader()
                  true
                )
                break

            unless found
              self.wnd.hideLoader()
              new UserMessage(I18n.tr('errors.player_not_found'), UserMessage.TYPE_ERROR).show()

            true
          ))

        textfieldName.getField().keypress((e) ->
          dlg.hide() if e.which == 13 && ok()
          true
        )

        dlg.setText(content).addButton('ok', ok).addButton('cancel').show()
        textfieldName.getField().focus()
        true

      onButtonAddClick: ->
        self = @
        dlg = new west.gui.Dialog(I18n.tr('dialogs.add_item'))
        content = $('<div style="margin-top: 10px; text-align: center;"><div id="skills-calc-preview-item" style="height: 60px; width: 60px; float: right; border: 1px solid black; border-radius: 4px;" /></div>')
        content.append((textfieldId = Gui.createTextfield(size: 25, label: I18n.tr('labels.item_id'))).maxlength(8).getMainDiv())

        textfieldId.getItem = ->
          id = parseInt(@getValue())
          if isNaN(id) then undefined else ItemManager.get(id)

        ok = ->
          if item = textfieldId.getItem()
            self.addItems([item])
            self.repaint()
            true
          else
            false

        dlg.setText(content).addButton('ok', ok).addButton('cancel').show()

        textfieldId.getField().keypress((e) ->
          dlg.hide() if e.which == 13 && ok()
          true
        ).keyup((e) ->
          preview = $('#skills-calc-preview-item', content).empty()

          if (item = textfieldId.getItem())
            item = new tw2widget.InventoryItem(item)
            preview.append($(item.getMainDiv()).css(float: 'none'))

          true
        ).focus()
        true

      onButtonResetClick: ->
        @reset()

      onCheckboxBonusClick: (state) ->
        @repaint()

      onCheckboxSkillsClick: (state) ->
        @recalc()
        @repaint()

      addItems: (items) ->
        self = @

        for item in items
          @removeItem(item)

          @items[item.type] = new tw2widget.InventoryItem(item)

          $(@items[item.type].getMainDiv()).css(float: 'none', display: 'inline-block').on('click', (e) ->
            unless e.shiftKey
              item = ItemManager.get($(e.target).data('itemId'))
              self.repaint() if item && self.removeItem(item)
            true
          )

          @calculator.sumItem(item)
          @groupItems.appendToScrollContentPane($(@items[item.type].getMainDiv()))
        true

      removeItem: (item) ->
        if @items[item.type]
          @calculator.subItem(@items[item.type].obj)
          $(@items[item.type].getMainDiv()).remove()
          delete @items[item.type]
          true
        else
          false

      recalc: ->
        @calculator.reset()
        for type, item of @items
          @calculator.sumItem(item.obj)

        if @checkboxSkills.isSelected()
          for skill, value of CharacterSkills.skills
            @calculator.calcSkill(skill, value.points, ItemCalculator.SUM_OP)
        true

      repaint: ->
        result = if @checkboxBonus.isSelected() then @calculator.resultWithBonus() else @calculator.result
        for skill, value of result
          if @images[skill]
            $("span.skills-calc-skillpoints_label", @images[skill]).text(value)

        min_health = @calculator.level * 10 + 90
        health =
          normal: format_number(min_health + result.health * 10)
          solder: format_number(min_health + result.health * 15)
          solderBonus: format_number(min_health + result.health * 20)
          toString: ->
            "#{@normal} / #{@solder} / #{@solderBonus}"

        @labelHealth.text("#{I18n.tr('labels.health')}: #{health.toString()}")
        true

      reset: ->
        for type, item of @items
          $(item.getMainDiv()).remove()

        $("span.skills-calc-skillpoints_label", @groupSkills.getMainDiv()).text('0')

        @items = {}
        @calculator.reset()
        @calculator.level = Character.level
        @textfieldLevel.setValue(Character.level)
        @labelHealth.text("#{I18n.tr('labels.health')}: 0 / 0 / 0")
        @checkboxBonus.setSelected(false, true)
        @checkboxSkills.setSelected(false, true)
        true

    class ItemCalculator
      @SUM_OP: 'sum'
      @SUB_OP: 'sub'

      constructor: ->
        @level = 1
        @_bonusExtractor = new west.item.BonusExtractor(level: @level)
        @_resultStack = []

        @reset()

      sum: (skill, value) ->
        @result[skill] += value

      sub: (skill, value) ->
        @result[skill] -= value

      sumItem: (item) ->
        @calcItem(item, ItemCalculator.SUM_OP)

      subItem: (item) ->
        @calcItem(item, ItemCalculator.SUB_OP)

      calcItem: (item, operator) ->
        @calcAttributes(item.bonus.attributes, operator) if item.bonus.attributes
        @calcSkills(item.bonus.skills, operator) if item.bonus.skills
        @calcBonuses(item.bonus.item, operator) if item.bonus.item
        @calcDifference(item.bonus.item, item.item_level, operator) if item.item_level > 0
        @calcSet(item.set, operator) if item.set
        @result

      calcAttribute: (attr, value, operator) ->
        skills = CharacterSkills.skillKeys4Attr[attr]
        for skill in skills
          @[operator](skill, value)
        @result

      calcAttributes: (attrs, operator) ->
        for attr, value of attrs
          @calcAttribute(attr, value, operator) if @isAttribute(attr)
        @result

      calcSkill: (skill, value, operator) ->
        @[operator](skill, value)

      calcSkills: (skills, operator) ->
        for skill, value of skills
          @calcSkill(skill, value, operator) if @isSkill(skill)
        @result

      calcDifference: (bonuses, itemLevel, operator) ->
        for bonus in bonuses
          diff = @_bonusExtractor.getCharacterItemValueDifferenceToItemLevel(bonus, 0, itemLevel)
          continue if diff <= 0

          switch bonus.bonus.type
            when 'attribute'
              @calcAttribute(bonus.bonus.name, diff, operator)
            when 'skill'
              @calcSkill(bonus.bonus.name, diff, operator)

        @result

      calcBonuses: (bonuses, operator) ->
        @_bonusExtractor.character.level = @level

        for bonus in bonuses
          @calcSkills(@_bonusExtractor.getAffectedSkills(bonus), operator)

        @result

      calcSet: (set, operator) ->
        switch operator
          when ItemCalculator.SUM_OP
            @itemSets[set] = (@itemSets[set] || 0) + 1
          when ItemCalculator.SUB_OP
            delete @itemSets[set] if (@itemSets[set] && (@itemSets[set] -= 1) <= 0)

        @result

      isAttribute: (attr) ->
        CharacterSkills.allAttrKeys.indexOf(attr) != -1

      isSkill: (skill) ->
        CharacterSkills.allSkillKeys.indexOf(skill) != -1

      reset: ->
        @itemSets = {}
        @result = {}

        for skill in CharacterSkills.allSkillKeys
          @result[skill] = 0

        @result

      resultWithBonus: ->
        @pushResult()
        @_bonusExtractor.level = @level

        bonus = {}

        for setId, itemCount of @itemSets
          itemSet = west.storage.ItemSetManager.get(setId)
          for i in [1..itemCount]
            stage = itemSet.bonus[i]
            if stage
              for b in stage
                switch b.type
                  when 'attribute', 'skill'
                    bonus[b.type] ||= {}
                    bonus[b.type][b.name] = (bonus[b.type][b.name] || 0) + b.value
                  when 'character'
                    bonus[b.type] ||= []
                    found = false

                    for cb, i in bonus[b.type]
                      found = cb.type == b.bonus.type && cb.name == b.bonus.name && cb.key == b.key && cb.roundingMethod == b.roundingMethod
                      if found
                        bonus[b.type][i].value += b.bonus.value
                        break

                    unless found
                      bonus[b.type].push(
                        type: b.bonus.type
                        name: b.bonus.name,
                        value: b.bonus.value
                        key: b.key
                        roundingMethod: b.roundingMethod
                      )

        @calcAttributes(bonus.attribute, ItemCalculator.SUM_OP) if bonus.attribute
        @calcSkills(bonus.skill, ItemCalculator.SUM_OP) if bonus.skill

        if bonus.character
          for b in bonus.character
            if b.key == 'level'
              value = @_bonusExtractor.getRoundedValue(b.value * @level, b.roundingMethod)
              switch b.type
                when 'attribute'
                  @calcAttribute(b.name, value, ItemCalculator.SUM_OP)
                when 'skill'
                  @calcSkill(b.name, value, ItemCalculator.SUM_OP)

        result = @result
        @popResult()
        result

      pushResult: ->
        @_resultStack.push($.extend(true, {}, @result))

      popResult: ->
        @result = @_resultStack.pop()

  $(document).ready(->
    api = TheWestApi.register(
      SkillsCalc.ID,
      SkillsCalc.NAME,
      SkillsCalc.MIN_GAME_VERSION,
      SkillsCalc.MAX_GAME_VERSION,
      SkillsCalc.AUTHOR,
      SkillsCalc.WEB_SITE
    )
    api.setGui("Copyrights, changelog and other details see <a href='#{SkillsCalc.WEB_SITE}' target='_blank'>here</a>.")
    calc = null

    EventHandler.listen(['itemmanager_loaded', 'itemsetmanager_loaded'], ->
      calc = new SkillsCalc() if calc == null && ItemManager.isLoaded() && west.storage.ItemSetManager.isLoaded()
      EventHandler.ONE_TIME_EVENT
    )
    true
  )
  true
))
